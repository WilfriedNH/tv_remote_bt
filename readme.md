# Bluetooth TV Remote

2014 - W.Hudson

## The Arduino part

### Hardware

(Tested on Arduino Leonardo)

- BT Vcc --> Arduino 3.3V
- BT GND --> Arduino GND
- BT RX --> Arduino TX (Pin 1)
- BT TX --> Arduino RX (Pin 0)
- IR Led (+) --> Arduino pin 12
- IR Led (-) --> Arduino GND

### Software

Use the Serial connection to send a specialcode and toggle an action.

(These are not RC5 codes but "shortcuts").

You are free to add your own ones.

- 201 : Power on / off
- 202 : Volume - x20
- 203 : Volume + x20
- 204 : Power on/off, then mute

Any other code will be directly sent usingthe RC5 format.

(For example, send 1 for channel 1).

## The Android part

A small .apk program is provided to use the remote from an Android smartphone.

It was designed with the excellent "MIT App Inventor 2".

You can either use this one or make your own.

