/*******************************************

Bluetooth TV Remote
2014 - W.Hudson
https://bitbucket.org/WilfriedNH/tv_remote_bt

-- Hardware --

(Tested on Arduino Leonardo)

BT Vcc     --> Arduino 3.3V
BT GND     --> Arduino GND
BT RX      --> Arduino TX (Pin 1)
BT TX      --> Arduino RX (Pin 0)
IR Led (+) --> Arduino pin 13
IR Led (-) --> Arduino GND

-- Software --

Use the Serial connection to send a special
code and toggle an action.
(These are not RC5 codes but "shortcuts").
You are free to add your own ones.

201 : Power on / off
202 : Volume - x20
203 : Volume + x20
204 : Power on/off, then mute

Any other code will be directly sent using
the RC5 format. (For example, send 1 for
channel 1).

*******************************************/
#include <IRremote.h>

IRsend irsend;

String code = "";

int ledPin = 13;

void setup(){
  Serial.begin(9600);
  Serial1.begin(9600);
  pinMode(ledPin, OUTPUT);
}

void irSend(int codeToSend) {
  irsend.sendRC5(codeToSend, ledPin);
  Serial.print("Generated code ");
  Serial.println(codeToSend);
}

void sendLoop(int codeToSend, int times, int msecdelay=300) {
  for (int i = 0; i < times; i++) {
    irSend(codeToSend);
    delay(msecdelay);
  }
}

void powerOnOff() {
  irSend(12);
  delay(200);
  irSend(33);
}
  
void loop(){
  if (Serial1.available() > 0) {
    int num = Serial1.read();
    Serial.print("Received code ");
    Serial.println(num);
    switch (num) {
      case 201:
        powerOnOff();
        break;
      case 202:
        sendLoop(17, 20);
        break;
      case 203:
        sendLoop(16, 20);
        break;
      case 204:
        powerOnOff();
        delay(2000);
        irSend(13);
        break;
      default:
        irSend(num);
        break;
    }
  }
}
